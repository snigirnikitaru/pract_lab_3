// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "lab3GameMode.generated.h"

UCLASS(minimalapi)
class Alab3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Alab3GameMode();
};



