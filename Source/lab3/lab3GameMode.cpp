// Copyright Epic Games, Inc. All Rights Reserved.

#include "lab3GameMode.h"
#include "lab3HUD.h"
#include "lab3Character.h"
#include "UObject/ConstructorHelpers.h"

Alab3GameMode::Alab3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Alab3HUD::StaticClass();
}
